## 基础知识
> [基本项目通识](./basics/%E9%A1%B9%E7%9B%AE%E5%BC%80%E5%8F%91%E9%80%9A%E8%AF%86.md)
>
> [java线程](./%E7%BA%BF%E7%A8%8B/thread.md)
>
> [java锁机制](./%E9%94%81%E6%9C%BA%E5%88%B6/%E9%94%81.md)

## spring

> [spring扩展点](./spring/%E9%80%9A%E8%BF%87Feign%E6%B3%A8%E5%86%8C%E4%BA%86%E8%A7%A3spring%E7%9B%B8%E5%85%B3%E6%89%A9%E5%B1%95.md)
>
> [事务扩展点](./spring/%E4%BA%8B%E5%8A%A1.md)
>
> [缓存](./spring/cache.md)

## 操作系统

> [操作系统](./%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F%E7%9B%B8%E5%85%B3/linux.md)
>
> [零拷贝](./%E9%9B%B6%E6%8B%B7%E8%B4%9D/%E9%9B%B6%E6%8B%B7%E8%B4%9D.md)

## 中间件

> [redis](./redis/redis.md)
>
> [nginx](./nginx/nginx.md)
>
> [postgre](./database/postgre.md)
>
> [kafka](./kafka/kafka.md)
>
> [quartz](./quartz/%E9%80%9A%E8%BF%87quartz%E4%BA%86%E8%A7%A3%E6%89%A9%E5%B1%95%E4%B9%8B%E7%BB%A7%E6%89%BF.md)

## 部署

> [docker](./docker/docker.md)

## 其他

> [流媒体](./%E6%B5%81%E5%AA%92%E4%BD%93/%E6%B5%81%E5%AA%92%E4%BD%93.md)
>
> [他人的学习笔记](https://www.yuque.com/u424962/tkuzvq/nwaxn4)
>
> [md文件数学公式](https://www.jianshu.com/p/25f0139637b7)
>
> [md文件教学](https://markdown.com.cn/basic-syntax/lists.html)