### 1、创建原始表（用于对照）
```sql
    CREATE TABLE qh_na_record ( na_time timestamp);
```
### 2、将原始表创建为分区表
```sql
    CREATE TABLE qh_na_record ( na_time timestampt) PARTITION BY RANGE (na_time);
```
### 3、创建分区
```sql
    create table fq_2 PARTITION of qh_na_record for values FROM ('2022-08-01') to ('2022-9-1')
```
### 4、摘出分区
```sql
    alter table qh_na_record detach PARTITION fq_2
```
### 5、挂载分区
```sql
    alter table qh_na_record attach PARTITION fq_2 for values FROM ('2022-08-01') to ('2022-9-1')
```
### 6、创建索引
```sql
    create INDEX fq_2_na_time_index on fq_2 USING btree(na_time)
    ALTER TABLE public.tablename      -- 添加主键的表名
        ADD CONSTRAINT tablename_pkey -- 主键的名称
        PRIMARY KEY (id); -- 主键的列名 
    CREATE UNIQUE INDEX name ON table (column [, ...]);
    DROP INDEX "public"."tt";
```
### 7、查询索引
```sql
    select 
        relname, indexrelname, idx_scan, idx_tup_read, idx_tup_fetch 
    from
        pg_stat_user_indexes
    where
        relname = 'fq_2'
    order by
        idx_scan asc, idx_tup_read asc, idx_tup_fetch asc;
```

### 8、用户管理
```sql
-- 查看某用户的系统权限
SELECT * FROM  pg_roles WHERE rolname='postgres';

-- 查看某用户的表级别权限
select * from information_schema.table_privileges where grantee='postgres';

-- 查看某用户的usage权限
select * from information_schema.usage_privileges where grantee='postgres';

-- 查看某用户在存储过程函数的执行权限
select * from information_schema.routine_privileges where grantee='postgres'; 

-- 查看某用户在某表的列上的权限
select * from information_schema.column_privileges where grantee='postgres'; 

-- 查看当前用户能够访问的数据类型
select * from information_schema.data_type_privileges ; 

-- 查看用户自定义类型上授予的USAGE权限
select * from information_schema.udt_privileges where grantee='postgres';


--创建具有登录权限的组角色，如：
create  role  username  login;

--创建数据库超级用户，如：
create role  username superuser;

--创建具有创建数据库权限的组角色，如
create role  username createdb;
 
--创建具有创建角色权限的角色，如：
create role username createrole;

--创建具有口令权限的角色，如：
create role test login password '123456';

/**
1、SELECT
2、INSERT
3、UPDATE
4、DELETE
5、TRUNCATE
6、REFERENCES
7、TRIGGER
8、CREATE
9、CONNECT
10、TEMPORARY
11、EXECUTE
12、USAGE
**/
-- 用户设置对scheam的使用权限
grant USAGE on SCHEMA public to test ;

-- 用户对COMPANY表的所有权限
GRANT ALL ON COMPANY TO test;

-- 撤销用户scheam的使用权限
REVOKE ALL ON SCHEMA public FROM test;


```


### 9、聚合分组

```sql
    -- 按逗号分隔
    SELECT string_agg(cdate_tno, ',')cdate_tno,arch_id FROM t_month_bill GROUP BY arch_id

    -- 取分组中第一条
    SELECT 
        table_field,
        count(*),
        max(id) -- 通过in查出来的id，过滤数据
    FROM "table_name" 
    GROUP BY table_field
```