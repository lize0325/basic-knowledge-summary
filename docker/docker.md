 *作者：明天什么不存在*


 ## 一、docker安装
> [官网安装指引](https://docs.docker.com/engine/install/centos/)

## 二、docker的一些概念

### 1.0 镜像和容器
 &emsp;&emsp;__镜像__ 与 __容器__ 的关系就想是 __类__ 与 __对象__ 的关系一样，一个是共享的模板，一个是实际运行的实例。
#### 1.0.1 相关命令
```shell
    docker images #查看镜像
    docker ps #查看正在运行的容器
    docker container ls #查看容器
    docker logs xxxx-container-name #查看日志
    docker exec -i -t  my-container /bin/bash #进入容器与其交互，退出不影响容器
    docker run \ #容器生成并启动
    -p 8001:80 \ #端口映射
    -v /logs:/logs \ #容器卷
    --name turing \ #容器名称
    --restart always \ #自动重启
    --network bridge \ #选择网络拓扑结构
    -d \ #守护程序
    turing:3 \ #选择镜像
```
### 2.0 外部文件挂载与卷挂载
&emsp;&emsp;要想将运行时产生的文件保留，可以通过三种方式处理，一种是将服务器的某个文件挂载到容器上，指定映射路径`-v 命令`；另一种是默认卷，由`Dockerfile`文件`VOLUME`命令生成，这将会在`docker/valumes/`路径下生成一个匿名卷，与容器中的指定的文件映射;还有一种是也是卷挂载，不过需要手动创建卷`docker run --name=DATA1 --volume=/srv busybox true`，通过`--volumes-from DATA1(卷名):/xxx`。

### 3.0 网络
&emsp;&emsp;分三类网络，以`--network bridge`命令指定。
- 无网络：不互相通信。
- bridge：以服务器为网关，生成了一个虚拟的子网络，`子网：172.17.0.0/16 网关：172.17.0.1`。
- host：与服务器共用网络。
```
docker network create --driver 网络类型 --subnet 网段 --gateway 网关 -o parent=通讯网卡 网络名
```

## 三、idea+docker的使用
### 1.0 服务器中docker配置
- 修改/usr/lib/systemd/system/docker.service。
```shell
ExecStart=/usr/bin/dockerd --containerd=/run/containerd/containerd.sock #删除-H fd:// 后的命令，防止和daemon.json的冲突
```
- 创建`/etc/docker/daemon.json`,开启端口2333，并配置证书路径。
```json
{
  "hosts": ["fd://","unix:///var/run/docker.sock","tcp://0.0.0.0:2333"],

  "tls": true,
  "tlscacert":"/var/lib/docker/CA/ca.pem",
  "tlscert":"/var/lib/docker/CA/server-cert.pem",
  "tlskey":"/var/lib/docker/CA/server-key.pem",
  "tlsverify": true
}
```
- 生成证书，将证书放到/var/lib/docker/CA下。
> [证书脚本(ca.sh host)](https://gitee.com/bai-yu-rolling/turing-multi/blob/master/doc/docker/ca.sh)

### 2.0 idea中docker的配置
- 连接服务器配置证书。
![连接服务器](./ideadocker.png )
- 编写Dockerfile,位于项目根部，下方是最简单的配置。
```
    FROM openjdk:16
    #数据卷，当运行命令忘记映射路径时，就会生成一个默认数据卷与logs映射，路径在/var/lib/docker/volumes下
    VOLUME  /logs
    COPY target/*.jar app.jar
    ENTRYPOINT ["java","-jar","/app.jar","--server.port=80"]
```
- 启动部署配置，以下是最终效果，运行即可部署。
![启动与部署](./dp.png )

- 注意没有证书的情况使用tcp://xxxx:xx

## 四、docker compose
```
docker compose up -d
```
>[docker-compose.yml配置](https://zhuanlan.zhihu.com/p/387840381)