#!/bin/bash

if [[ "$*" = *"/test-1.sh"* || "$*" = *"test-1.sh"* ]]; then
    echo "** Starting Redis setup **"

    echo "** Redis setup finished! **"
fi

echo "$@"

exec "$@"
