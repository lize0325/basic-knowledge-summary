
## 一、术语
+ Producer：消息⽣产者，向 Kafka Broker 发消息的客户端。
+ Consumer：消息消费者，从 Kafka Broker 取消息的客户端。Kafka支持持久化，生产者退出后，未消费的消息仍可被消费。
+ Consumer Group：消费者组（CG），消费者组内每个消费者负责消费不同分区的数据，提⾼消费能⼒。⼀个分区只能由组内⼀个消费者消费，消费者组之间互不影响。所有的消费者都属于某个消费者组，即消费者组是逻辑上的⼀个订阅者。
+ Broker：⼀台 Kafka 机器就是⼀个 Broker。⼀个集群(kafka cluster)由多个 Broker 组成。⼀个 Broker 可以容纳多个 Topic。
+ Controller：由zookeeper选举其中一个Broker产生。它的主要作用是在 Apache ZooKeeper 的帮助下管理和协调整个 Kafka 集群。
+ Topic：可以理解为⼀个队列，Topic 将消息分类，⽣产者和消费者⾯向的是同⼀个 Topic。
+ Partition：为了实现扩展性，提⾼并发能⼒，⼀个⾮常⼤的 Topic 可以分布到多个 Broker上，⼀个 Topic 可以分为多个 Partition，同⼀个topic在不同的分区的数据是不重复的，每个 Partition 是⼀个有序的队列，其表现形式就是⼀个⼀个的⽂件夹。不同Partition可以部署在同一台机器上，但不建议这么做。
+ Replication：每⼀个分区都有多个副本，副本的作⽤是做备胎。当主分区（Leader）故障的时候会选择⼀个备胎（Follower）上位，成为Leader。在kafka中默认副本的最⼤数量是10个，且副本的数量不能⼤于Broker的数量，follower和leader绝对是在不同的机器，同⼀机器对同⼀个分区也只可能存放⼀个副本（包括⾃⼰）。
+ Message：每⼀条发送的消息主体。
+ Leader：每个分区多个副本的“主”副本，⽣产者发送数据的对象，以及消费者消费数据的对象，都是 Leader。
+ Follower：每个分区多个副本的“从”副本，使用发布订阅模式主动拉取Leader的数据（与redis不同），实时从 Leader 中同步数据，保持和 Leader 数据的同步。Leader 发⽣故障时，某个 Follower 还会成为新的 Leader。
+ Offset：消费者消费的位置信息，监控数据消费到什么位置，当消费者挂掉再重新恢复的时候，可以从消费位置继续消费。
+ ZooKeeper：Kafka 集群能够正常⼯作，需要依赖于 ZooKeeper，ZooKeeper 帮助 Kafka存储和管理集群信息。
+ High Level API 和Low Level API ：高水平API，kafka本身定义的行为，屏蔽细节管理，使用方便；低水平API细节需要自己处理，较为灵活但是复杂。

## 二、docker启动
```shell
docker run -d --name kafka \
-p 9099:9099 \
-v /mnt/sdc/kafka/logs:/opt/kafka/logs \
-v /mnt/sdc/kafka:/kafka \
-v /etc/localtime:/etc/localtime:ro \
-e KAFKA_BROKER_ID=1 \
-e KAFKA_ZOOKEEPER_CONNECT=172.17.0.1:2188 \
-e KAFKA_ADVERTISED_LISTENERS=INSIDE://:9092,OUTSIDE://47.94.233.207:9099 \
-e KAFKA_LISTENER_SECURITY_PROTOCOL_MAP=INSIDE:PLAINTEXT,OUTSIDE:PLAINTEXT \
-e KAFKA_LISTENERS=INSIDE://:9092,OUTSIDE://:9099 \
-e KAFKA_INTER_BROKER_LISTENER_NAME = INSIDE \
-e KAFKA_LOG_DIRS=/kafka/kafka-logs \
-t wurstmeister/kafka:2.12-2.3.1
```

## 三、网络配置
> [网络配置](https://www.szzdzhp.com/kafka/op/brokerServerConfig.html#security-inter-broker-protocol)

注意一个问题
inter.broker.listener.name=监听器名称
根据本地配置的监听器名称, 去查找其他Broker的监听器的EndPoint。
所以一般所有Broker的监听器名称都必须一致，否则的话就找不到具体的EndPoint,无法正确的发起请求。
也就是说，原先已经搭建好的节点，也需要改为外网，来支持节点之间的通信

## 四、配置详解

>[配置文件详解](https://blog.csdn.net/huxiaodong1994/article/details/118273869)
