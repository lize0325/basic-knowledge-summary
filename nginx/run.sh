docker run -d --net=host --cap-add NET_ADMIN \
--name keepalived \
-e KEEPALIVED_AUTOCONF=true                  \
-e KEEPALIVED_STATE=MASTER                   \
-e KEEPALIVED_INTERFACE=enp2s0                 \
-e KEEPALIVED_VIRTUAL_ROUTER_ID=2            \
-e KEEPALIVED_TRACK_INTERFACE_1=enp2s0        \
-e KEEPALIVED_VIRTUAL_IPADDRESS_1="172.10.1.200/24 dev enp2s0" \
arcts/keepalived