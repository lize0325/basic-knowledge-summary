 *作者：明天什么不存在*

 &emsp;&emsp;**前情提要：项目中使用到了quartz，通过cronTrigger触发器，周期性的执行相关任务，但有一个问题，cron不能处理某一天的多个时间点，例如：我需要周一至周天上午八点四十分和下午一点三十分执行，这个场景cron就做不到，于是我想通过扩展cronTrigger的方式去实现。**

 ## 一、找到扩展点
 &emsp;&emsp;__正规的开源项目，一般都会有规定的接口和抽象模板（简化接口）。__
 ### 1.0 找到其中一个具体的实现
&emsp;&emsp;**因为这里我想通过cronTrigger扩展，于是我就找了cronTrigger的实例cronTriggerImpl。**
![实例生成](./builder.png )
![实例生成](./b2.png )
![实例生成](./impl.png )

### 2.0 分析CronTriggerImpl接口
&emsp;&emsp;**首先分析我们需要什么，此次扩展，我想控制触发时间，实现一天触发多个时间点的功能，那么我们需要留意相关的触发时间的方法，也就是说，要浏览一遍接口，大致知道其功能。下图给出了需要关注的接口，其中重点观察的方法中，大部分需要`getTimeAfter`方法，也就是说官方给了扩展方法，只需要继承重写即可。**
![扩展](./kz.png )

## 二、继承与重写

### 1.0 `Date getTimeAfter(Date)`方法的抽象意义
&emsp;&emsp;**理解方法的抽象意义是首要的前提，我一开始就因为理解错了这个方法的含义，而走错了方向，在计算上就会出现错误。这个接口的含义是：给定一个时间，无论这个时间是否是上一次执行时间，求其下一次执行的时间。**
![扩展](./ex.png )
### 2.0 builder的重写
&emsp;&emsp;**这一块就是粘贴复制cron的，重写`build`更换具体实现。**
![扩展](./b3.png )
# 三、项目

> [项目测试文件连接](https://gitee.com/bai-yu-rolling/turing-multi/blob/master/turing-web/src/test/java/com/turing/core/config/QuartzTest.java)