## 一、配置模块
> [配置详解](https://blog.csdn.net/weixin_44236477/article/details/120339537)
+ 开头说明
+ INCLUDES 配置文件引入
+ MODULES 自定义模块配置
+ NETWORK 网络配置
+ GENERAL 通用配置（日志、默认数据库、pid、守护进程）
+ SNAPSHOTTING RDB持久化配置（写入数据）
+ REPLICATION 副本复制相关
+ SECURITY 安全相关
+ CLIENTS 客户端配置
+ MEMORY MANAGEMENT 内存管理、内存满了LRU算法
+ APPEND ONLY MODE AOF持久化（写入命令）
+ LUA SCRIPTING lua脚本基本配置
+ REDIS CLUSTER 集群配置

## 二、docker-compose + redis
> [Redis+docker](https://github.com/bitnami/containers/tree/main/bitnami/redis)
>
> [哨兵](https://github.com/bitnami/containers/tree/main/bitnami/redis-sentinel)
>
> [集群，注意node.conf文件，如果ip地址改变，要修改其中的ip，或者删除，让redis重建](https://github.com/bitnami/containers/tree/main/bitnami/redis-cluster)
>
> [集群部署非docker](https://blog.csdn.net/qq_41432730/article/details/121591008)
## 三、相关命令
```
    docker启动一主二从三哨兵 docker compose -f docker-compose-sentinel.yml up -d --scale redis-sentinel=3
```
```
    集群创建 redis-cli --cluster create 47.94.233.207:7000 47.94.233.207:7001 47.94.233.207:7002 47.94.233.207:7003 47.94.233.207:7004 47.94.233.207:7005 --cluster-replicas 1
```
```
    集群信息 cluster info
    节点信息 cluster nodes
``` 

## 四、 怎么让相关的数据落到同一个节点上？

在 key 里面加入 {hash tag} 即可。Redis 在计算槽编号的时候只会获取 {} 之间的字符串进行槽编号计算，这样由于上面两个不同的键，{} 里面的字符串是相同的，因此他们可以被计算出相同的槽

    user{2673}base=…
    user{2673}fin=…

## redis 扩容与缩容
>https://www.jianshu.com/p/ef833fba6c31
>https://blog.csdn.net/Brave_heart4pzj/article/details/126503130

## redis win
>https://github.com/tporadowski/redis/releases
