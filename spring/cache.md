## springboot+cache

- 1、声明式

```java
    @Cacheable(cacheNames = {"city"},key = "'all'")
    @Override
    public List<City> getAllForTree() {
        
    }
```


> [注解的相关应用](https://blog.csdn.net/zl1zl2zl3/article/details/110987968)

- 2、编程式

```java
    @Autowired
    private CacheManager cacheManager;


    public List<City> getAll(){
        // 对比声明注解
        Cache c = cacheManager.getCache("city");
        //自动反序列化：json->object
        c.get("all").get();
    }
```

## 缓存击穿
*缓存中没有但数据库中有的数据，假如是热点数据，那key在缓存过期的一刻，同时有大量的请求，这些请求都会击穿到DB，造成瞬时DB请求量大、压力增大。*

```java
 //通过同步锁
 @Cacheable(cacheNames = {"city"},key = "'all'",sync=true)
```

## 缓存雪崩
*大量的Key设置了相同的过期时间，导致缓存在同一时刻全部失效，造成瞬时DB请求量增大，压力骤增，引起雪崩。*

```
过期时间不要相同，可以设置的多个缓存管理器。
```

## 缓存穿透

*查询一个不存在的数据，缓存是不命中的，请求到达了数据库中。*

```
cache-null-values: true
缓存空值，默认选项
```



## 缓存带来的问题
> [缓存一致性问题](https://blog.csdn.net/bocai_xiaodaidai/article/details/124010396)


