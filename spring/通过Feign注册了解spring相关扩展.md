## Feign启动流程

- __@EnableFeignClients__ 通过 __@Import__ 注解注入 __FeignClientsRegistrar.class__

- __FeignClientsRegistrar.class__ 实现 __ImportBeanDefinitionRegistrar__

- 通过 __ClassPathScanningCandidateComponentProvider__ 扫描指定注解的接口也就是 __@FeignClient__ 

- 根据注解+**FactoryBean**生成相应的bean，最后通过 __BeanDefinitionReaderUtils.registerBeanDefinition(definitionHolder, registry)__ 注册到 __IOC__

> [@Import的三种用法（直接导入config配置、ImportSelector、ImportBeanDefinitionRegistrar）](https://blog.csdn.net/hylwan8884658/article/details/109537092)

## spring扩展点

### 1、BeanFactoryPostProcessor
_BeanFactoryPostProcessor是用来处理BeanFactory中Bean属性的后置处理器，也就是说在Bean初始化之前，Spirng提供了一个钩子可以让你根据自己的实际情况修改Bean的属性。_

```java
public class MyBeanFactoryPostProcessor implements BeanFactoryPostProcessor {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        System.out.println("调用自定义BeanFactoryPostProcessor");
        BeanDefinition beanDefinition = beanFactory.getBeanDefinition("user");
        System.out.println("开始修改属性的值");
        beanDefinition.getPropertyValues().add("userName","Tom");

    }
}
```

### 2、BeanDefinitionRegistryPostProcessor
_该扩展点可以让应用程序注册自定义的BeanDefinition，并且该扩展点在 BeanFactoryPostProcessor前执行，继承BeanFactoryPostProcessor。_ 

```java
@Component
public class P implements BeanDefinitionRegistryPostProcessor {
    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        // 创建一个bean的定义类的对象
        //BeanDefinition类，https://blog.elkfun.com/?p=1473
        RootBeanDefinition rootBeanDefinition = new RootBeanDefinition(C.class);
        // 将Bean 的定义注册到Spring环境
        registry.registerBeanDefinition("c", rootBeanDefinition);
        //第一、先调用这个注册c
        System.out.println("postProcessBeanDefinitionRegistry----注册c");
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        // bean的名字为key, bean的实例为value
        // 第二、当bean都完成注册后，调用这个方法
        // 这里只是做好了bean 的定义，但是没有真正的如，初始化bean和bean的注入
        String[] strings = beanFactory.getBeanDefinitionNames();
        System.out.println("postProcessBeanFactory----start");
        for (String string : strings) {
            System.out.println(string);
        }
        System.out.println("postProcessBeanFactory----end");
    }
}
```

### 3、Aware
_Aware 接口是一个标记接口，表示所有实现该接口的类是会被Spring容器选中，并得到某种通知。所有该接口的子接口提供固定的接收通知的方法，例如如下。_

- **EnvironmentAware** 接收环境变量

- **ApplicationContextAware**

- **BeanFactoryAware**

- **BeanNameAware**

- [ResourceLoaderAware接收ResourceLoader处理资源](https://blog.csdn.net/qq_16992475/article/details/121581159)。


### 4、ApplicationListener
```java
public class SystemListener implements ApplicationListener<ContextRefreshedEvent> {

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        if (event.getApplicationContext().getParent() == null) {
            System.out.println("do something");
        }
    }

}
```

##### Spring内置事件
- 1、ContextRefreshedEvent

ApplicationContext 被初始化或刷新时，该事件被发布。这也可以在 ConfigurableApplicationContext接口中使用 refresh() 方法来发生。此处的初始化是指：所有的Bean被成功装载，后处理Bean被检测并激活，所有Singleton Bean 被预实例化，ApplicationContext容器已就绪可用
- 2、ContextStartedEvent

当使用 ConfigurableApplicationContext （ApplicationContext子接口）接口中的 start() 方法启动 ApplicationContext 时，该事件被发布。你可以调查你的数据库，或者你可以在接受到这个事件后重启任何停止的应用程序
- 3、ContextStoppedEvent

当使用 ConfigurableApplicationContext 接口中的 stop() 停止 ApplicationContext 时，发布这个事件。你可以在接受到这个事件后做必要的清理的工作
- 4、ContextClosedEvent

当使用 ConfigurableApplicationContext 接口中的 close() 方法关闭 ApplicationContext 时，该事件被发布。一个已关闭的上下文到达生命周期末端；它不能被刷新或重启
- 5、RequestHandledEvent

这是一个 web-specific 事件，告诉所有 bean HTTP 请求已经被服务。只能应用于使用DispatcherServlet的Web应用。在使用Spring作为前端的MVC控制器时，当Spring处理用户请求结束后，系统会自动触发该事件

> [事件的创建与发布applicationContext.publishEvent(e)](https://blog.csdn.net/x123453316/article/details/120684497)

### 5、InitializingBean
_InitializingBean 主要用来实现自定义的Bean初始化逻辑。 InitializingBean接口的方法会被BeanFactory在BeanFactoryAware或ApplicationContextAware接口方法调用后进行调用。_

*InitializingBean 接口的功能还有一个替代方式，就是在xml配置bean结点属性的init-method属性，指定初始化方法。需要注意的时候如同同时实现了InitializingBean接口并且配置了init-method属性，则InitializingBean接口的方法会被先执行。*

**构造方法 > @PostConstruct >InitializingBean#afterPropertiesSet() > init方法**

### 6、BeanPostProcessor

```java
public class MyBeanPostProcessor implements BeanPostProcessor{

	/**
	 * 实例化、依赖注入完毕，在调用显示的初始化之前完成一些定制的初始化任务
	 * 注意：方法返回值不能为null
	 * 如果返回null那么在后续初始化方法将报空指针异常或者通过getBean()方法获取不到bena实例对象
	 * 因为后置处理器从Spring IoC容器中取出bean实例对象没有再次放回IoC容器中
	 */
	@Override
	public Object postProcessBeforeInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("初始化 before--实例化的bean对象:"+bean+"\t"+beanName);
		// 可以根据beanName不同执行不同的处理操作
		return bean;
	}

	/**
	 * 实例化、依赖注入、初始化完毕时执行 
	 * 注意：方法返回值不能为null
	 * 如果返回null那么在后续初始化方法将报空指针异常或者通过getBean()方法获取不到bena实例对象
	 * 因为后置处理器从Spring IoC容器中取出bean实例对象没有再次放回IoC容器中
	 */
	@Override
	public Object postProcessAfterInitialization(Object bean, String beanName) throws BeansException {
		System.out.println("初始化 after...实例化的bean对象:"+bean+"\t"+beanName);
		// 可以根据beanName不同执行不同的处理操作
		return bean;
	}

}
```

### 7、FactoryBean
*区别于BeanFactory，其继承类ApplicationContext，我们通过bean的名称或者类型都可以从BeanFactory来获取bean。*

*当我们使用第三方框架或者库时，有时候是无法去new一个对象的，
比如静态工厂，对象是不可见的，只能通过getInstance（）之类方法获取，
此时就需要用到FactoryBean,通过实现FactoryBean接口的bean重写getObject（）方法，
返回我们所需要的bean对象。*

- FactoryBean包装一个对象，使用getObject()方法来获取真正的实例化对象
- FactoryBean包装的对象一般来自第三方，无法添加@Component等注解，就无法直接被Spring管理
- 这些对象是已经被实例化了的，有统一的来源，例如来自Mybatis
- 当然也可以自定义一个bean，使用FactoryBean来包装，但这好像是多余的。@Bean注解可以很好的解决需要自定义实例化、初始化过程的bean

> @Bean有局限性，第一你得在配置类生命，第二你你需要set的参数如何传递，比如有的是动态参数  FactoryBean更加的灵活，不需要配置类声明，只要能注册到ioc就行，@Bean主要是偏向不太复杂的Bean构造，比如你在业务中就可以使用，FactoryBean主要是偏向更加复杂的Bean构造，在其他框架整合spring中用的比较多，比如mybatis和feign都是通过FactoryBean来实现的，你可以翻一下他们的源码，看看用@Bean容易不，对比一下，就知道他的应用场景了。
作者：zzyang
链接：https://www.zhihu.com/question/537405375/answer/2526632599
来源：知乎
著作权归作者所有。商业转载请联系作者获得授权，非商业转载请注明出处。

> 举个简单例子，mybatis-spring
框架就是扫描mapper文件夹，每个mapper都注册成bean type为原class，实际为MapperFactoryBean类型（实现FactoryBean接口），最终getObject方法返回代理类的bean。
这种情况下你不知道有多少mapper，无法为每个mapper写一个@Bean方法。